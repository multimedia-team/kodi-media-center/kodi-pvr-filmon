# Kodi Media Center language file
# Addon Name: PVR Filmon Client
# Addon id: pvr.filmon
# Addon Provider: Stephen Denham
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:11+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Basque (Spain) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-filmon/eu_es/>\n"
"Language: eu_es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "Filmon PVR Client"
msgstr "Filmon PVR Bezeroa"

msgctxt "Addon Description"
msgid "Filmon frontend. Supports live TV, recordings, EPG. Requires a Filmon subscription. Before enabling: (a) in Filmon set channels as favourite to see them in Kodi (b) enter your username and password in this addon's settings. Note: recordings can take several minutes to appear after timer is completed."
msgstr ""

msgctxt "#30000"
msgid "Username"
msgstr "Erabiltzailea"

msgctxt "#30001"
msgid "Password"
msgstr "Pasahitza"

msgctxt "#30002"
msgid "Prefer HD Streams"
msgstr ""

msgctxt "#30003"
msgid "Only load favourite channels"
msgstr ""
